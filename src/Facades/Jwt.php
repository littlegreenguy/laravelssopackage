<?php
namespace Microlise\SSO\Facades;

use Illuminate\Support\Facades\Facade;
use Illuminate\Support\Facades\Log;
use Microlise\SSO\Classes\JwtHelpers;

class Jwt extends Facade{

    protected static function getFacadeAccessor() { return 'jwt'; }

}

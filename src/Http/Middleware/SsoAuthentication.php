<?php
namespace Microlise\SSO\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Microlise\SSO\Classes\Interfaces\SsoInterface;

class SsoAuthentication
{
    public function __construct(SsoInterface $ssoRepo)
    {
        $this->ssoRepo = $ssoRepo;
    }

    public function handle($request, Closure $next)
    {
        if (config('sso.sso_enabled'))
        {
            //check for response from SSO
            if( $request->post('code') &&
                $request->post('id_token') &&
                $request->post('access_token')
            ){
                try{
                    $this->ssoRepo->processNewToken($request->post('access_token'), $request->post('id_token'));
                } catch(\Exception $e) {
                    Log::info('processNewToken failed: ' . $e->getMessage());
                }
            }

            //TODO - add further security checks for access token/validity etc
            if(
                !session('sub')
            ){
                $query = $this->ssoRepo->buildSsoQueryStr();
                return redirect(config('sso.sso_login_url') . '?returnUrl=' . $query);
            }
        }

        return $next($request);
    }

    public function getRequestPath($request)
    {
        $protocol = $request->getProtocolVersion();
        $protocol = explode('/', $protocol)[0];
        $requestPath = strtolower($protocol) . '://' . $request->getHttpHost() . $request->getRequestUri();

        return$requestPath;
    }
}

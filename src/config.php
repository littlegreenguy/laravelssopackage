<?php

$sso_identity_service_url = env('SSO_IDENTITY_SERVICE_URL', 'http://identityservice.int.honey.mms.local/');

return [

    'sso_enabled'   => env('SSO_ENABLED', true),
    'sso_client_id' => env('SSO_CLIENT_ID', 'knowledgeportal'),
    'sso_identity_service_url'   => $sso_identity_service_url,

    'sso_login_url'      => env('SSO_AUTH', $sso_identity_service_url . 'identity/account/login'),
    'sso_redirect'  => env('SSO_REDIRECT', config('app.url') . '/callback'),
    'sso_return_url' => env('SSO_RETURN_URL', '/identity/connect/authorize/callback'),
    'sso_home_route_name' => env('SSO_HOME_ROUTE_NAME', 'home'),

    'sso_token'     => env('SSO_TOKEN', $sso_identity_service_url . 'identity/account/token'),
    'sso_keys'      => env('SSO_KEYS', $sso_identity_service_url . 'identity/account/keys'),

];

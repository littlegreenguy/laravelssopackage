<?php

Route::match(array('GET', 'POST'), '/callback', function () {
    return \Illuminate\Support\Facades\Redirect::route('home');
})
    ->middleware(\Microlise\SSO\Http\Middleware\SsoAuthentication::class)
    ->name('callback');

//Forget Session - re-authenticate
if (env('APP_ENV') != 'prod') {
    Route::get('/forget', function ($request) {
        $request->session()->flush();
        echo 'session & cache cleared';
        return redirect()->route('home');
    });
}

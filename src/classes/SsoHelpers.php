<?php
if (!function_exists('setLocalSsoVar')) {
    function setLocalSsoVar($envVar)
    {
        $setting = env('SSO_ENV_LOCAL') ? env($envVar . '_LOCAL') : env($envVar);
        return $setting;
    }

    function ifArrayMakeStr($input){
        return !is_array($input) ? $input : implode(',', $input);
    }
}
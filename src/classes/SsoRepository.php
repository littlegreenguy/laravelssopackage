<?php

namespace Microlise\SSO\Classes;

use Microlise\SSO\Classes\Interfaces\SsoInterface;
use Lcobucci\JWT\Token;
use Microlise\SSO\Facades\Jwt;

use App\Models\Activity;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Response;


class SsoRepository implements SsoInterface {

    private $access_token;
    private $id_token;

    /**
     * ***********
     *  SSO LOGIN
     * ***********
     */
    public function buildSsoQueryStr()
    {
        $query1 = urlencode(config('sso.sso_return_url') . '?');

        $query = [];
        $query[] = 'client_id=' . config('sso.sso_client_id');
        $query[] = 'redirect_uri=' . config('sso.sso_redirect');
        $query[] = 'response_type=code%20id_token%20token';
        $query[] = 'scope=openid%20profile';
        $query[] = 'state=' . time();
        $query[] = 'nonce=' . time();
        $query[] = 'response_mode=form_post';

        $query = implode('&', $query);
        $query = urlencode($query);

        return $query1 . $query;
    }

    /**
     * ******************
     *  TOKEN PROCESSING
     * ******************
     */

    /**
     * Retrieve token from SSO callback:
     * - Validate token
     * - Store credentials in session
     * - Get activities and roles from AAS
     */
    public function processNewToken($access_token, $id_token)
    {
        $this->access_token = $access_token;
        $this->id_token = $id_token;

        try{
            $this->storeInfoFromToken();
        } catch(\Exception $e) {
            throw $e;
        }

        return true;
    }

    /**
     * If token is valid, store info from token into session
     */
    public function storeInfoFromToken()
    {
        try{
            $access_obj = $this->parseToken($this->access_token);
            $id_obj     = $this->parseToken($this->id_token);
        } catch(\Exception $e){
            throw $e;
        }

        Session::put('sub', $access_obj->getClaim('sub'));
//        Session::put('roles', $id_obj->getClaim('role'));
        Session::put('access_token', $this->access_token);
        Session::put('logged_in_as', $id_obj->getClaim('given_name') . ' ' . $id_obj->getClaim('family_name'));

        return true;
    }

    /**
     * validate security credentials
     * parse token into object
     * check token claims are all present
     */
    public function parseToken(string $token)
    {
        try{
            $token_obj = Jwt::parse($token);
            Jwt::validate($token_obj);
        } catch (\Exception $e){
            Log::channel('sso_stack')
                ->error('Token Parse error: ' . $e->getMessage());
            throw $e;
        }

        return $token_obj;
    }

    /**
     * Extra validation to check if claim is present
     */
    public function validateToken(Token $token, array $expectedKeys)
    {
        foreach ($expectedKeys as $key){
            if(!$token->getClaim($key)){
                throw new \Exception('Token missing claim: ' . $key);
            }
        }
        return true;
    }
}

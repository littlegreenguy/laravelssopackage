<?php

namespace Microlise\SSO\Classes;

use Illuminate\Support\Facades\Log;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\ValidationData;
use Lcobucci\JWT\Builder;


class JwtHelpers {

    public function parse($token)
    {
        $token = (new Parser())->parse((string) $token); // Parses from a string
        return $token;
    }

    /**
     * Check token credentials
     *
     * Additional validation if required:
     *
     * $data->setAudience('http://example.org');
     * $data->setId('45BD69658B6F2A5BDF72A115D3B7E3BF9CA13AF3');
     * $data->setNotBefore('4f1g23a12aa');
     * $data->setExpiration('4f1g23a12aa');
     */
    public function validate($token)
    {
        $data = new ValidationData(); // It will use the current time to validate (iat, nbf and exp)
        $data->setIssuer(config('sso.sso_identity_service_url') . 'identity');

        if( !$token->validate($data) ){
            Log::channel('sso_stack')
                ->info('Invalid Token: ' . $token->getClaim('iss'));
            throw new \Exception('Invalid Token');
        }
        return true;
    }

    public function dummy($issuer=null, $toString=true)
    {
        $token = (new Builder())
            ->setIssuer($issuer ?? config('sso.sso_identity_service_url') . 'identity') // iss
            ->setAudience('http://example.org') // aud
            ->setId('4f1g23a12aa', true) //
            ->setIssuedAt(time()) // Configures the time that the token was issued (iat claim)
            ->setNotBefore(time()) // nbf
            ->setExpiration(time() + 3600) // exp
            ->set('sub', '54ffcb33-c21b-4a9e-a191-914946c782f6')
            ->set('given_name', 'Nicholas')
            ->set('client_id', 'bluetoothpwgen')
            ->set('family_name', 'Cage')
            ->set('role', [
                "9feee4d8-16dc-46ea-aafe-98dbb48923e9",
                "09e9296d-4fb0-408f-b6f3-3ea52f4a205f",
                "7a762ce5-dab4-4be3-904b-8c18e1a9893a"
            ])
            ->getToken();
            $token = $toString ? $token->__toString() : $token;

        return $token;
    }
}

<?php

namespace Microlise\SSO\Providers;

use Illuminate\Support\ServiceProvider;

class MicroliseSSOPackageServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('Microlise\SSO\Classes\Interfaces\SsoInterface', 'Microlise\SSO\Classes\SsoRepository');
        $this->app->bind('jwt', function()
        {
            return new \Microlise\SSO\Classes\JwtHelpers;
        });
        $this->mergeConfigFrom(__DIR__.'/../logging.php', 'logging.channels');
    }

    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/../routes.php');
        $this->publishes([
            __DIR__.'/../config.php' => config_path('sso.php'),
        ]);
    }
}

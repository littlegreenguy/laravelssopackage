<?php

return [
    'channels' => [
        'sso_stack' => [
            'driver' => 'stack',
            'channels' => ['sso', 'slack']
        ],

        'sso' => [
            'driver' => 'daily',
            'path' => storage_path('logs/sso_message_log.log'),
            'level' => 'debug',
            'days' => 14,
        ],
    ]
];

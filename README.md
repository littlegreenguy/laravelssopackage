# Microlise Laravel SSO Package

This package facilitates application authentication via Microlise SSO

Package Installation
--------------------

Install by adding to your `composer.json`:

```
"require": {
    "microlise/microlise-laravel-sso-package": "dev-master"
},
"repositories": [
    {
        "type": "git",
        "url": "https://tfs.microlise.com/tfs/DefaultCollection/Microlise/_git/MicroliseLaravelSSOPackage"
    }
],

```

Once Installed, run:

```php artisan vendor:publish --provider="Microlise\SSO\Providers\MicroliseSSOPackageServiceProvider"```


This will create an ```sso.php``` file in ```/config``` 

#### Settings

```sso_enabled``` - this will toggle the application between SSO authentication and and other configured standard app authentication
